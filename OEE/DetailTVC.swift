//
//  DetailTVC.swift
//  OEE
//
//  Created by Andrew on 22.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit

class DetailTVC: UITableViewController {
    @IBOutlet weak var avalibleTimeTexfField: UITextField!
    @IBOutlet weak var breakTexfField: UITextField!
    @IBOutlet weak var changeoverTexfField: UITextField!
    @IBOutlet weak var unplanedDTTexfField: UITextField!
    @IBOutlet weak var meetingTexfField: UITextField!
    @IBOutlet weak var planedDTTexfField: UITextField!
    @IBOutlet weak var idealCycleTimeTexfField: UITextField!
    @IBOutlet weak var totalOutPutTexfField: UITextField!
    @IBOutlet weak var scrapTexfField: UITextField!
    @IBOutlet weak var avalibleTimeLable: UILabel!
    @IBOutlet weak var breakLable: UILabel!
    @IBOutlet weak var changeoverLable: UILabel!
    @IBOutlet weak var unplanedDTLable: UILabel!
    @IBOutlet weak var meetingTimeLable: UILabel!
    @IBOutlet weak var planedDTLable: UILabel!
    @IBOutlet weak var cycletimeLable: UILabel!
    @IBOutlet weak var totalOutPutLable: UILabel!
    @IBOutlet weak var scrapPartsLable: UILabel!
    
    var resultTeoreticalTime = 0.0
    var resultRuningTime = 0.0
    var resultNetOperatingTime = 0.0
    var resultUsableTime = 0.0
    var resultProductiveTime = 0.0
    var qualityResult = 0.0
    var perfomanceResult = 0.0
    var aviliabilityResult = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundCourners()
        self.tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:))))
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let chartVC = segue.destination as? Chart else {return}
        chartVC.teoreticalTime = self.resultTeoreticalTime
        chartVC.planedProductionTimes = self.resultRuningTime
        chartVC.operatingTimeCalc = self.resultNetOperatingTime
        chartVC.usableTimePerShift = self.resultUsableTime
        chartVC.fullyProductiveTime = self.resultProductiveTime
        chartVC.aviliability = Int(aviliabilityResult)
        chartVC.perfomance = Int(perfomanceResult)
        chartVC.quality = Int(qualityResult)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    // MARK: - Main calculation from field
    
    func calculation() {
        teoreticalTime()
        runingTime()
        netOperatingTime()
        usableTime()
        productiveTime()
        oeeCalculation()
    }
    
    // MARK: - Calculation teoreticaltime, how many we have per shift/day
    
    func teoreticalTime() {
        let result = Double(avalibleTimeTexfField.text!)!
        resultTeoreticalTime = result
    }
    
    // MARK: - Calculation running time (Teoretical time - planed downtime)
    
    func runingTime() {
        let runingTime = Double(avalibleTimeTexfField.text!)! - Double(breakTexfField.text!)! - Double(planedDTTexfField.text!)!
        resultRuningTime = runingTime
        print("runingTime - \(resultRuningTime)")
    }
    
     // MARK: - Calculation net operating time (running time - all unplaned downtime)
    
    func netOperatingTime() {
        let operationTime = resultRuningTime - (Double(changeoverTexfField.text!)! + Double(unplanedDTTexfField.text!)!)
        resultNetOperatingTime = operationTime
        print("resultNetOperatingTime - \(resultNetOperatingTime)")
        
    }
    
     // MARK: - Calculation usable time (net oeperating time - small losses)
    
    func usableTime() {
        let usableTime = (Double(totalOutPutTexfField.text!)! * Double(idealCycleTimeTexfField.text!)!) / 60.0
        let result = resultNetOperatingTime - usableTime
        resultUsableTime = resultNetOperatingTime - result
        print("resultUsableTime - \(resultUsableTime)")
        
    }
    
     // MARK: - Calculation productive time (usable time - total scrap parts)
    
    func productiveTime() {
        let quality = (Double(scrapTexfField.text!)! * Double(idealCycleTimeTexfField.text!)!) / 60.0
        let reslut = resultUsableTime - quality
        resultProductiveTime = reslut
        print("resultProductiveTime - \(resultProductiveTime)")
        
    }
    
    // MARK: - Main function to calculate OOE (E * P * Q = OEE)
    
    func oeeCalculation() {
        let quality = (resultProductiveTime / resultUsableTime) * 100
        qualityResult = quality
        print("qualityResult - \(qualityResult)")
        let perfomance = (resultUsableTime / resultNetOperatingTime) * 100
        perfomanceResult = perfomance
        print("perfpmanceResult \(perfomanceResult)")
        let aviliability = (resultNetOperatingTime / resultRuningTime) * 100
        aviliabilityResult = aviliability
        print("aviliabilityResult \(aviliabilityResult)")
        let oee = (aviliability * perfomance * quality) * 100
        
        print("OEE result = \(oee)")
    }
    
    // MARK: - Alert message
    
    func alert() {
        let alert = UIAlertController(title: "Alert", message: "Some of field is empty, please fullfil data", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action: UIAlertAction!) in }))
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Func to round corners
    
    func roundCourners() {
        avalibleTimeLable.setRoundEdge()
        breakLable.setRoundEdge()
        changeoverLable.setRoundEdge()
        unplanedDTLable.setRoundEdge()
        meetingTimeLable.setRoundEdge()
        planedDTLable.setRoundEdge()
        cycletimeLable.setRoundEdge()
        totalOutPutLable.setRoundEdge()
        scrapPartsLable.setRoundEdge()
    }
    
    // MARK: - Hide keyboard in table view
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            self.view.endEditing(true)
            for textField in self.view.subviews where textField is UITextField {
                textField.resignFirstResponder()
            }
        }
        sender.cancelsTouchesInView = false
    }
    
    // MARK: - Result button + check fiels if not empty
    
    @IBAction func result(_ sender: Any) {
        if (avalibleTimeTexfField.text?.isEmpty)! || (breakTexfField.text?.isEmpty)! || (changeoverTexfField.text?.isEmpty)! || (unplanedDTTexfField.text?.isEmpty)! || (meetingTexfField.text?.isEmpty)! || (planedDTTexfField.text?.isEmpty)! || (idealCycleTimeTexfField.text?.isEmpty)! || (totalOutPutTexfField.text?.isEmpty)! || (scrapTexfField.text?.isEmpty)! {
            alert()
        }
        else {
            calculation()
            performSegue(withIdentifier: "buildData", sender: self)
        }
    }

}


