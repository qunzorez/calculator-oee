//
//  Chart.swift
//  OEE
//
//  Created by Andrew on 12.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit
import Foundation
import Charts

class Chart: UIViewController {
    
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var allTimes: UILabel!
    @IBOutlet weak var planedTime: UILabel!
    @IBOutlet weak var runTimes: UILabel!
    @IBOutlet weak var productionTimeLable: UILabel!
    @IBOutlet weak var fullyProductiveTimeLable: UILabel!
    @IBOutlet weak var viewMorDataButton: UIButton!
    
    var quality = 0
    var aviliability = 0
    var perfomance = 0
    var teoreticalTime = 0.0
    var planedProductionTimes = 0.0
    var operatingTimeCalc = 0.0
    var usableTimePerShift = 0.0
    var fullyProductiveTime = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawMarginChart()
        textGenerator()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let chartVC = segue.destination as? DetailChartVC else {return}
        chartVC.teoreticalTime = self.teoreticalTime
        chartVC.planedProductionTimes = self.planedProductionTimes
        chartVC.operatingTimeCalc = self.operatingTimeCalc
        chartVC.usableTimePerShift = self.usableTimePerShift
        chartVC.fullyProductiveTime = self.fullyProductiveTime
    }
    
    // MARK: - Function to build chart from previus VC data
    
    func drawMarginChart() {
        
        let values = [aviliability, perfomance, quality]
        let labels = ["Aviliability", "Perfomance", "Quality"]
        var dataEntries = [ChartDataEntry]()
        for i in 0..<values.count {
            let entry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
            
            dataEntries.append(entry)
        }
        
        let barChartDataSet = BarChartDataSet(values: dataEntries, label: "")
        barChartDataSet.drawValuesEnabled = true
        barChartDataSet.colors = ChartColorTemplates.joyful()
        let barChartData = BarChartData(dataSet: barChartDataSet)
        barChart.data = barChartData
        barChart.legend.enabled = false
        barChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
        barChart.xAxis.granularityEnabled = true
        barChart.xAxis.granularity = 1
        barChart.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInOutBounce)
        barChart.chartDescription?.text = ""
        let rightAxis = barChart.rightAxis
        rightAxis.drawGridLinesEnabled = true
        let leftAxis = barChart.leftAxis
        leftAxis.drawGridLinesEnabled = false
        let xAxis = barChart.xAxis
        xAxis.drawGridLinesEnabled = true
        barChart.setVisibleXRange(minXRange: 3.0, maxXRange: 3.0)
        barChart.leftAxis.axisMinimum = 0
        barChart.setExtraOffsets (left: 0, top: 20.0, right:0.0, bottom: 20.0)
        barChart.xAxis.labelFont = UIFont.systemFont(ofSize: 5)
        barChart.xAxis.labelTextColor = UIColor.red

    }
    
    // MARK: - Round lable corners
    
    func roundCourners() {
        allTimes.setRoundEdge()
        planedTime.setRoundEdge()
        runTimes.setRoundEdge()
        productionTimeLable.setRoundEdge()
        fullyProductiveTimeLable.setRoundEdge()
        viewMorDataButton.layer.masksToBounds = true
        viewMorDataButton.layer.cornerRadius = 5
    }
    
    // MARK: - Generator description in text field
    
    func textGenerator() {
        allTimes.text = "All time avalible \(Int(teoreticalTime))min - \((teoreticalTime / 60).rounded())h "
        planedTime.text = "Planed production time \(Int(planedProductionTimes))min - \((planedProductionTimes / 60).rounded())h"
        runTimes.text = "Avalible time \(Int(operatingTimeCalc))min - \((operatingTimeCalc / 60).rounded())h"
        productionTimeLable.text = "Running time \(Int(usableTimePerShift))min - \((usableTimePerShift / 60).rounded())h"
        fullyProductiveTimeLable.text = "Fully productive time \(Int(fullyProductiveTime))min - \((fullyProductiveTime / 60).rounded())h"
       roundCourners()
    }
    
    // MARK: - Button which bulde big chart with all alvalible data
    
    @IBAction func moreInfoButton(_ sender: Any) {
         performSegue(withIdentifier: "shoVCTwo", sender: self)
    }
    
}
