//
//  ShowSavedDataTVC.swift
//  OEE
//
//  Created by Andrew on 28.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit
import Parse
import Charts

class ShowSavedDataTVC: UITableViewController {
    
    var data = [PFObject]()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    // MARK: - Fucn to fetch data from server
    
    func fetchData() {
        let query = PFQuery(className: "savedResult")
        query.order(byDescending: "createdAt")
        query.limit = 10
        query.findObjectsInBackground { (objects, error) in
            if let goods = objects {
                self.data = goods
                self.tableView.reloadData()
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data.count
    }

    // MARK: - Button build chart, and check data in fields
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "goodCell") as! CustomCellTVC
        
        let good = data[indexPath.row]
        // Configure the cell...
 
        cell.totalAvalibleTimeNumberLable.text = (good["teoreticalTime"] as? Int ?? 1).description
        cell.avalibleTimeNumberLable.text = (good["planedProductionTimes"] as? Int ?? 1).description
        cell.runingTimeNumberLable.text = (good["operatingTimeCalc"] as? Int ?? 1).description
        cell.usableTimeNumberLable.text = (good["usableTimePerShift"] as? Int ?? 1).description
        cell.productiveTimeNumberLable.text = (good["fullyProductiveTime"] as? Int ?? 1).description
        
        return cell
    }
}
