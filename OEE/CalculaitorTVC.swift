//
//  CalculaitorTVC.swift
//  OEE
//
//  Created by Andrew on 22.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit
import Parse

class CalculaitorTVC: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var picerView: UIPickerView!
    @IBOutlet weak var plannedDowntimeField: UITextField!
    @IBOutlet weak var unplannedDowntimeField: UITextField!
    @IBOutlet weak var totalPartsProducedField: UITextField!
    @IBOutlet weak var idealCycleTimeField: UITextField!
    @IBOutlet weak var totalScrapField: UITextField!
    @IBOutlet weak var hideLable: UILabel!
    @IBOutlet weak var alivLbl: UILabel!
    @IBOutlet weak var perfLbl: UILabel!
    @IBOutlet weak var qualLbl: UILabel!
    @IBOutlet weak var oeeLbl: UILabel!
    @IBOutlet weak var resultOeeButn: UIButton!
    @IBOutlet weak var buildChartBtn: UIButton!
    @IBOutlet weak var saveResultItem: UIBarButtonItem!
    
    let shift = ["12 hours", "8 hour"]
    var shiftModel = 0.0
    var plannedProductionTime = 0.0
    var operatingTimeCalc = 0.0
    var oeeResult = 0
    var teoreticalTime = 0.0
    var planedProductionTimes = 0.0
    var avalibleTime = 0.0
    var runingTime = 0.0
    var usableTimePerShift = 0.0
    var fullyProductiveTime = 0.0
    var qualityResult = 0
    var aviliabilityResult = 0
    var perfomanceResult = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundCorner()
        countRunApp()
        self.tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:))))
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        view.endEditing(true)
        return 3
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let chartVC = segue.destination as? Chart else {return}
        chartVC.teoreticalTime = self.teoreticalTime
        chartVC.planedProductionTimes = self.planedProductionTimes
        chartVC.operatingTimeCalc = self.operatingTimeCalc
        chartVC.usableTimePerShift = self.usableTimePerShift
        chartVC.fullyProductiveTime = self.fullyProductiveTime
        chartVC.aviliability = self.aviliabilityResult
        chartVC.perfomance = self.perfomanceResult
        chartVC.quality = self.qualityResult
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return shift[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return shift.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        hideLable.text = shift[row]
    }
    
    // MARK: - Shift time calculation, in minutes
    
    private func calculateShiftTime () {
        if hideLable.text == "8 hour" {
            let shiftOne = 9.0
            let minutes = 60.0
            let result = shiftOne * minutes
            shiftModel = result
            teoreticalTime = shiftModel
            
        }
        else {
            let shiftTwo = 13.0
            let minutes = 60.0
            let result = shiftTwo * minutes
            shiftModel = result
            teoreticalTime = shiftModel
            
        }
    }
    
    // MARK: - Calculation planed production time (shift model - all planed downtime)
    
    private func planedProductionTime() {
        let result =  shiftModel - Double(plannedDowntimeField.text!)!
        plannedProductionTime = result
        planedProductionTimes = plannedProductionTime
    }
    
    // MARK: - Operating time - it's time how many was planed by logistic minus planed DT
    
    private func operatingTime() {
        let result = plannedProductionTime - Double(unplannedDowntimeField.text!)!
        operatingTimeCalc = result
        avalibleTime = operatingTimeCalc
    }
    
    // MARK: - Calculation performance operator/line/ (total output parts per ideal cycle time and we resive time with losses)
    
    private func performCalc() {
        let usableTime = (Double(totalPartsProducedField.text!)! * (Double(idealCycleTimeField.text!)! / 60))
        usableTimePerShift = usableTime
        let result = avalibleTime - usableTime
        runingTime = result
    }
    
    // MARK: - Quality calculation, how many time we waste to fix the scrap parts(total scrap parts * ideal cycle time - usable time per shift)
    
    private func qualityu() {
        let timeForScrap = (Double(totalScrapField.text!)! * (Double(idealCycleTimeField.text!)! / 60))
        let result = usableTimePerShift - timeForScrap
        fullyProductiveTime = result
    }
    
    // MARK: - Main OOE calculation (A * P * Q = OOE)
    
    private func calculation() {
        let aviviability = operatingTimeCalc / planedProductionTimes
        alivLbl.text = "Aviviability = \(Int(aviviability * 100))%"
        aviliabilityResult = Int(aviviability * 100)
        let perfomance = usableTimePerShift / operatingTimeCalc
        perfLbl.text = "Perfomance = \(Int(perfomance * 100))%"
        perfomanceResult = Int(perfomance * 100)
        let quality = fullyProductiveTime / usableTimePerShift
        qualLbl.text = "Quality = \(Int(quality * 100))%"
        qualityResult = Int(quality * 100)
        let oee = aviviability * perfomance * quality
        oeeLbl.text = "OEE = \(Int(oee * 100))%"
        oeeResult = Int(oee * 100)
    }
    
    // MARK: - func which make small calculation in backround
    
    private func makeCalculation() {
        calculateShiftTime()
        planedProductionTime()
        operatingTime()
        performCalc()
        qualityu()
    }
    
    // MARK: - alert message
    
    func alertWhenResultMoreThenValue() {
        let alert = UIAlertController(title: "Do you enter correct data?", message: "Please check the field, your OEE result equal \(oeeResult) \n Your perfomance result equal \(perfomanceResult) \n Your aviliability result equal \(aviliabilityResult) \n Your quality result equal \(qualityResult)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action: UIAlertAction!) in }))
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Block and unblock chart button if no data in text field
    
    func blockButtonAndUnBlock() {
        if plannedDowntimeField.text == "" || unplannedDowntimeField.text == "" || totalPartsProducedField.text ==
            "" || idealCycleTimeField.text == "" || totalScrapField.text == "" {
            alert(message: "Please write data in field", title: "Alert")
            buildChartBtn.isEnabled = false
            saveResultItem.isEnabled = false
        }
        else {
            makeCalculation()
            calculation()
            checkDownTime()
            buildChartBtn.isEnabled = true
            saveResultItem.isEnabled = true
        }
    }
    
    // MARK: - alert message if no data in field
    
    func alert(message: NSString, title: NSString) {
      let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Round corner
    
    func roundCorner() {
        alivLbl.setRoundEdge()
        perfLbl.setRoundEdge()
        qualLbl.setRoundEdge()
        oeeLbl.setRoundEdge()
    }
    
    // MARK: - Check result ooe, because standat of oee calculation say ooe can be more then 100%
    
    func checkValueOee() {
        if oeeResult > 100 || qualityResult > 100 || perfomanceResult > 100 || aviliabilityResult > 100 {
            alertWhenResultMoreThenValue()
        }
    }
    
    // MARK: - Check Down time field, if value is not biger the teoretical time
    
    func checkDownTime() {
        if Double(unplannedDowntimeField.text!)! > teoreticalTime || Double(plannedDowntimeField.text!)! > teoreticalTime {
            let alert = UIAlertController(title: "Alert", message: "Your time more then Teoretical time \n Teoretical time = \(teoreticalTime) \n Planed Down Time = \(Double(plannedDowntimeField.text!)!) \n Unplaned Down Time = \(Double(unplannedDowntimeField.text!)!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Count run time application, if run time = 1, you will see welcome screen
    
    func countRunApp() {
        let kRunCount = "kRunCount"
        var runCount = UserDefaults.standard.integer(forKey: kRunCount)
//        runCount = 0
        runCount += 1
        UserDefaults.standard.set(runCount, forKey: kRunCount)
        print("run\t#\(runCount)")
        
        if runCount == 1 {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScrenVC") as! WelcomeScrenVC
            self.present(vc,animated:true,completion:nil)
        }
    }
    
    // MARK: - Hide key board in table view
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            // Do your thang here!
            self.view.endEditing(true)
            for textField in self.view.subviews where textField is UITextField {
                textField.resignFirstResponder()
            }
        }
        sender.cancelsTouchesInView = false
    }
    
    // MARK: - Result buton to make all calculation
    
    @IBAction func resultButton(_ sender: Any) {
        blockButtonAndUnBlock()
        checkValueOee()
    }
    
    // MARK: - Button build chart, and check data in fields
    
    @IBAction func buildChart(_ sender: Any) {
        if plannedDowntimeField.text == "" || unplannedDowntimeField.text == "" || totalPartsProducedField.text ==
            "" || idealCycleTimeField.text == "" || totalScrapField.text == "" {
            alert(message: "Please write data in field", title: "Alert")
            buildChartBtn.isEnabled = false
        }
        else{
            performSegue(withIdentifier: "showVC", sender: self)
        }
    }
    
    // MARK: - Button to save data in back4app
    
    @IBAction func saveData(_ sender: Any) {
        let dataBase = PFObject(className: "savedResult")
        dataBase["teoreticalTime"] = Int(teoreticalTime)
        dataBase["planedProductionTimes"] = Int(planedProductionTimes)
        dataBase["operatingTimeCalc"] = Int(operatingTimeCalc)
        dataBase["usableTimePerShift"] = Int(usableTimePerShift)
        dataBase["fullyProductiveTime"] = Int(fullyProductiveTime)
        dataBase.saveEventually { (success, error) in
            if success {
                self.alert(message: "Please wait, your data saving", title: "Wait")
                print("successfuly saved ")
                self.saveResultItem.isEnabled = false
            }
            else {
                print(error!)
            }
        }
    }
}

