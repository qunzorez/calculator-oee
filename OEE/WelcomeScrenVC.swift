//
//  WelcomeScrenVC.swift
//  OEE
//
//  Created by Andrew on 20.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit

class WelcomeScrenVC: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var startUseButton: UIButton!
    
    
    var contentWidth:CGFloat = 0.0
    
    // MARK: - Welcome screen with scroll view use page controll
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.delegate = self
        startUseButton.isHidden = true
        startUseButton.layer.masksToBounds = true
        startUseButton.layer.cornerRadius = 5
        let myImages = ["intro1_1.png", "intro2_2.png", "intro3_3.png"]
        let imageWidth:CGFloat = view.frame.width
        let imageHeight:CGFloat = view.frame.height
        var xPosition:CGFloat = 0
        var scrollViewSize:CGFloat=0

        for image in myImages {
            let myImage:UIImage = UIImage(named: image)!
            let myImageView:UIImageView = UIImageView()
            myImageView.image = myImage
            myImageView.frame.size.width = imageWidth
            myImageView.frame.size.height = imageHeight
            myImageView.frame.origin.x = xPosition
            myImageView.frame.origin.y = 0
            scrollView.addSubview(myImageView)
            xPosition += imageWidth
            scrollViewSize += imageWidth
        }
        scrollView.contentSize = CGSize(width: scrollViewSize, height: imageHeight)
        scrollView.contentSize = CGSize(width: view.frame.width * 3, height: view.frame.height)
        
    }
    
    // MARK: - Action when user scroll pages, and show button "Start use" if user in page 2
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth / 2)/pageWidth)+1
        self.pageControll.currentPage = Int(currentPage)
        if currentPage == 2 {
            startUseButton.isHidden = false
            startUseButton.isEnabled = true
        } else {
            startUseButton.isHidden = true
        }
    }
    
    // MARK: - Segue to main screen
    
    func goTomainLoginScrren() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let introRootVC = storyBoard.instantiateInitialViewController()
        self.present(introRootVC!, animated: true, completion: nil)
    }
    
    // MARK: - Button which send user to main screen
    
    @IBAction func startUseButtonTapped(_ sender: Any) {
       goTomainLoginScrren()
    }
    
}
