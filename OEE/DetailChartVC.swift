//
//  DetailChartVC.swift
//  OEE
//
//  Created by Andrew on 14.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit
import Charts

class DetailChartVC: UIViewController {
    
    @IBOutlet weak var barChart: HorizontalBarChartView!
    @IBOutlet weak var avalobleTimeLable: UILabel!
    @IBOutlet weak var avalibleTimeLable: UILabel!
    @IBOutlet weak var runnungTime: UILabel!
    @IBOutlet weak var usableTime: UILabel!
    @IBOutlet weak var netTimeLable: UILabel!
    
    var teoreticalTime = 0.0
    var planedProductionTimes = 0.0
    var operatingTimeCalc = 0.0
    var usableTimePerShift = 0.0
    var fullyProductiveTime = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawMarginChart()
        textGenerator()
    }
    
    // MARK: - Function to build chart from previus VC data
    
    func drawMarginChart() {
        
        let values = [fullyProductiveTime, usableTimePerShift, operatingTimeCalc, planedProductionTimes, teoreticalTime]
        let labels = [""]
        var dataEntries = [ChartDataEntry]()
        for i in 0..<values.count {
            let entry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
            
            dataEntries.append(entry)
        }
        let barChartDataSet = BarChartDataSet(values: dataEntries, label: "")
        barChartDataSet.colors = ChartColorTemplates.joyful()
        let barChartData = BarChartData(dataSet: barChartDataSet)
        barChart.data = barChartData
        barChart.legend.enabled = false
        barChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
        barChart.xAxis.granularityEnabled = true
        barChart.xAxis.granularity = 0.5
        barChart.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInOutBounce)
        barChart.chartDescription?.text = ""
        let rightAxis = barChart.rightAxis
        rightAxis.drawGridLinesEnabled = true
        let leftAxis = barChart.leftAxis
        leftAxis.drawGridLinesEnabled = false
        let xAxis = barChart.xAxis
        xAxis.drawGridLinesEnabled = true
        barChart.setVisibleXRange(minXRange: 5.0, maxXRange: 5.0)
        barChart.leftAxis.axisMinimum = 0
        barChart.setExtraOffsets (left: 0, top: 10.0, right:0.0, bottom: 10.0)
        barChart.xAxis.labelFont = UIFont.systemFont(ofSize: 5)
        barChart.xAxis.labelTextColor = UIColor.red
        barChart.drawGridBackgroundEnabled = true
        barChart.data?.setDrawValues(false)
        
    }
    
    // MARK: - Generator description in text field
    
    func textGenerator () {
        avalobleTimeLable.text = "Total avalible time - \(Int(teoreticalTime))min"
        avalibleTimeLable.text = "Avalible time - \(Int(planedProductionTimes))min"
        runnungTime.text = "Running time - \(Int(operatingTimeCalc))min"
        usableTime.text = "Usable operating time - \(Int(usableTimePerShift))min"
        netTimeLable.text = "Net productive time - \(Int(fullyProductiveTime))min"
    }
    
}
