//
//  CustomCellTVC.swift
//  OEE
//
//  Created by Andrew on 28.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit
import Charts

class CustomCellTVC: UITableViewCell {
    @IBOutlet weak var totalAvalibleTimeLable: UILabel!
    @IBOutlet weak var totalAvalibleTimeNumberLable: UILabel!
    @IBOutlet weak var avalibleTimeLable: UILabel!
    @IBOutlet weak var avalibleTimeNumberLable: UILabel!
    @IBOutlet weak var runingTimeLable: UILabel!
    @IBOutlet weak var runingTimeNumberLable: UILabel!
    @IBOutlet weak var usableTimeLable: UILabel!
    @IBOutlet weak var usableTimeNumberLable: UILabel!
    @IBOutlet weak var productiveTimeLable: UILabel!
    @IBOutlet weak var productiveTimeNumberLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
       roundCorners()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Func to round corners
    
    func roundCorners() {
        totalAvalibleTimeNumberLable.setRoundEdge()
        totalAvalibleTimeLable.setRoundEdge()
        avalibleTimeLable.setRoundEdge()
        avalibleTimeNumberLable.setRoundEdge()
        runingTimeLable.setRoundEdge()
        runingTimeNumberLable.setRoundEdge()
        usableTimeLable.setRoundEdge()
        usableTimeNumberLable.setRoundEdge()
        productiveTimeLable.setRoundEdge()
        productiveTimeNumberLable.setRoundEdge()
    }

}

